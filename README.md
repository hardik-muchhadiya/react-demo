1) **create react project**
   - npx create-react-app react-demo
   - add package-lock.json in .gitignore.
   - update README.md
2) **Try to run and get a working Welcome Page.**
   - changes in app.js

3) **render data from objects**
   - make Main function component in main.js.
   - call Main component in app.js

4) **add style and css**
   - [Refrence Doc](https://www.w3schools.com/REACT/react_css_styling.asp)

5) **add bootstrap**
   - [Refrence Doc](https://react-bootstrap.github.io/getting-started/introduction/)

6) **Create array and mapping with li tag**
   - create list.js file in component folder.
   - call List component in app.js.

7) **task 9 using onChange**
   - create task9.js in pages folder and call in app.js

8) **task 10**
   - create task10.js in pages folder and call in app.js.

9) **task 11**
   - converter price*qty.
10) **task12**
    - Time Converter & Celsius to Fahrenheit converter with dropdown.
11) **task13**
    - Increment Decrement Value.
12)  **clone Increament Decrement**
     - task14 : create task14.js file in pages folder and call in App.js file.
13)  **packages**
      - check moment,axios,font awesome packages.
14)  **react paginate package**
      - npm install react-paginate --save
      - sudo npm install -g json-server.
      - JSON Placeholder for Free fake API for testing and prototyping.
      - make file pagination.json and call it. 
15)  **Lodash**
      - make l1.js component and call it.

16)   **MovieDB**
      - use axios

17)   **task16 useContext**
      - make use task16 folder and make component parent.js and child.js
      - make ParentR.js ChildR.js component in task16 folder for radio context
      - make ParentS.js ChildS.js component in task16 folder for select context
      - make ParentI.js ChildI.js component in task16 folder for input context

18)   **task16 using redux**
      - install react redux using npm install redux react-redux --save .
      - make actions,Reducers folder
      - create CheckBox.js component for checkbox task .
      - create Radiobox.js component for radiobox task.
      - make store.js file for create store.
      - create SelectBox.js component for selectbox task.
      - create InputBox.js component for Inputbox task.
      - task done check route '/redux-checkbox'.

19)  **Redux toolkit**
      - install redux-toolkit using below command.
          -npm install @reduxjs/toolkit react-redux
      - perform increment decrement task using redux toolkit. 

20)  **task16 Redux toolkit checkbox**
      - make TcheckBox.js component in pages.
      - make TradioBox.js component in pages folder and call it afetr TchechBox.js component.
      - make radioBoxSlice.js in redux-toolkit folder .
      - make TselectBox.js component in pages folder and call it afetr TradioBox.js component.
      - make selectBoxSlice.js in redux-toolkit folder .
      - make TinputBox.js component in pages folder and call it afetr TselectBox.js component.
      - make inputBoxSlice.js in redux-toolkit folder .