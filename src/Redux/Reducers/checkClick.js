const initialState={first:false,second:false,third:false};
const radioinitialState={first:false,second:false};
const defaultSelectValue={value:0};
const initialInputValue="";
const checkValue =(state=initialState,action)=>{
    // console.log('first',action);
    switch(action.type){
        case "checked": return action.object;

        default:return state;
    }
}
export default checkValue;
export const radiocheckValue =(state=radioinitialState,action)=>{
    // console.log('second',action);
    switch(action.type){
        case "radiochecked": return action.radio;
        
        default:return state;
    }
}
export const selectValue =(state=defaultSelectValue,action)=>{
    // console.log('select action',action);
    switch(action.type){
        case "selectOption": return action.select;
        
        default:return state;
    }

}
export const InputValue =(state=initialInputValue,action)=>{
    
    switch(action.type){
        case "input": return action.input;
        
        default:return state;
    }

}


