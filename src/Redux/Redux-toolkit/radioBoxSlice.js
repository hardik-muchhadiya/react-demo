import { createSlice } from '@reduxjs/toolkit'

const initialState = {
  value: {first:false,second:false}
}
export const radioSlice = createSlice({
    name: 'radio',
    initialState,
    reducers: {
      Radioclick: (state,action) => {
        console.log('redioclick',action.payload)
        state.value = action.payload;
      },
    },
  })
  
  export const { Radioclick } = radioSlice.actions
  
  export default radioSlice.reducer