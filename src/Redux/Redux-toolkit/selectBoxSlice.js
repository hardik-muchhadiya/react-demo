import { createSlice } from '@reduxjs/toolkit'

const initialState = {
  value: 0,
}
// console.log('initala option:',initialState.value);
export const selectSlice = createSlice({
  name: 'option',
  initialState,
  reducers: {
    selected: (state,action) => {
      console.log('stateaa',action.payload)
      state.value = action.payload;
    },
  },
})

// Action creators are generated for each case reducer function
export const {selected } = selectSlice.actions

export default selectSlice.reducer