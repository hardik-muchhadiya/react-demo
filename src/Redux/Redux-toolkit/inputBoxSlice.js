import { createSlice } from '@reduxjs/toolkit'

const initialState = {
  value: '',
}
console.log(' input initialvalue:',initialState.value)

export const inputSlice = createSlice({
  name: 'input',
  initialState,
  reducers: {
    typeInput: (state,action) => {
    // console.log('aaaafirst',action)
          state.value =action.payload.value;
    },
  },
})

// Action creators are generated for each case reducer function
export const { typeInput } = inputSlice.actions

export default inputSlice.reducer