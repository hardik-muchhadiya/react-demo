import { createSlice } from '@reduxjs/toolkit'

const initialState = {
  value: {first:false,second:false,third:false},
}
// console.log(' iniatial check value:',initialState.value);
export const CheckSlice = createSlice({
    name:'check',
    initialState,
    reducers:{
        checkInput:(state,action)=>{
            state.value = action.payload;
        }
    },
})
export const { checkInput} = CheckSlice.actions

export default CheckSlice.reducer