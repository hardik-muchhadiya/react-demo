import React from "react";
import {useDispatch,useSelector} from 'react-redux';
import {selectBox} from "../../Redux/actions/indexaction";
export default function SelectBox() {
  const SelectState = useSelector((state) => state.selectValue);
  const SelectDispatch = useDispatch();
  

  return (
    <>
    <hr/>
    <div style={{ marginLeft: "500px" }}  >
      <select onChange={(e)=>SelectDispatch(selectBox({...SelectState,value:e.target.value}))} value={SelectState} >
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
      </select>
    <h2>duplicate</h2>
    </div>
    <div style={{ marginLeft: "500px" }}>
      <select onChange={(e)=>SelectDispatch(selectBox({...SelectState,value:e.target.value}))} value={SelectState} >
        <option value="1"  >1</option>
        <option value="2">2</option>
        <option value="3">3</option>
      </select>
    </div>

    </>
    
  );
}
