import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { radioCheckBox } from "../../Redux/actions/indexaction";

export default function RadioBox() {
  const radioState = useSelector((state) => state.radiocheckValue);
  // console.log("radio mystate:", radioState);
  const Dispatch = useDispatch();
  function click(e, input) {
      if(e.target.checked){
      Dispatch(radioCheckBox({...radioState, ...input}));
    }
  }
  return (
    <>
      <hr />
      <div style={{ marginLeft: "500px" }}>
        <input
          type="radio"
          name="role"
          checked={radioState.first}
          onClick={(e) => click(e, {first: true, second: false})}
        />
        Developer
        <br />
        <input
          type="radio"
          name="role"
          checked={radioState.second}
          onClick={(e) => click(e, {first: false, second: true})}
        />
        Designer
        <br />
        <h3>duplicate</h3>
        <input
          type="radio"
          name="role1"
          checked={radioState.first}
          onClick={(e) => click(e, {first: true, second: false})}
        />
        Developer
        <br />
        <input
          type="radio"
          name="role1"
          checked={radioState.second}
          onClick={(e) => 
            click(e, {first: false, second: true})}
        />
        Designer
        <br />
      </div>
    </>
  );
}
