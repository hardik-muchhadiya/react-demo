import React, { useState } from 'react'

export default function Task9() {
    const [name,setName] = useState('');
    function changeInput(prep){
        console.log(prep);
        setName(prep.target.value);
    }

    return (
    <>
        <div>Task9</div>
        <input type="text" name="name" onChange={changeInput} />
        <h1>{name}</h1>
    </>
  )
}
