import React, { useEffect, useState } from "react";
import "./movie.css";
import axios from "axios";
import { Button } from "react-bootstrap";
import { Link } from "react-router-dom";
import { MOVIEDB_API_KEY,MOVIEDB_BASE_URL,MOVIEDB_IMG_URL } from "../../utils/constant";
export default function Movie() {
  var API_URL = MOVIEDB_BASE_URL + "/discover/movie?sort_by=popularity.desc&" + MOVIEDB_API_KEY;
  var [page, setPage] = useState(1);
  const [movie, setMovie] = useState([]);
  useEffect(() => {
    axios
      .get(API_URL + "&page=" + page)
      .then(function (response) {
        setMovie([...movie, ...response.data.results]);
      })
      .catch(function (error) {
        console.log(error);
      })
      .then(function () {});
  }, [page]);

  return (
    <>
      <header>
        <h3 className="movieDB_title">movieDB</h3>

        <form id="form">
          <input
            type="text"
            placeholder="Search..."
            id="search"
            className="search"
          />
        </form>
      </header>

      <main id="main">
        {movie.map((m) => {
          return (
            <div id="movieclone">
              <Link to={`/movieDetail?id=${m.id}`} className="m_anchor">
                <img
                  src={MOVIEDB_IMG_URL + m.poster_path}
                  alt="Error"
                  className="m_img"
                />
                <div className="movie-info">
                  <h3 className="m_title">{m.title}</h3>
                  <span className="m_rating green">{m.vote_average}</span>
                </div>
                <div className="m_overview ">
                  <h3 className="m_overview_h3">overview</h3>
                  <p className="m_overview_p">{m.overview}</p>
                </div>
              </Link>
            </div>
          );
        })}
      </main>
      <Button className="my-3 button " onClick={() => setPage(page + 1)}>
        Load More
      </Button>
    </>
  );
}
