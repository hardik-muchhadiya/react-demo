import React, { useState } from "react";
import { Button } from "react-bootstrap";

export default function Task14() {
  const [clones, setClone] = useState([<div></div>]);
  function setIncreament(event) {
    var val = event.target.nextSibling;
    val.value = parseInt(val.value) + 1;
  }
  function setDecrement(e) {
    var val = e.target.previousSibling;
    val.value = parseInt(val.value) - 1;
  }
  let cloneDiv = (e) => {
    setClone([
      ...clones,
      <div>
        <Button onClick={setIncreament}>+</Button>
        <input type="text" className="text-center" value="0" />
        <Button onClick={setDecrement}>-</Button>
      </div>,
    ]);
  };
  return (
    <div>
      <div>Task14</div>
      <Button onClick={cloneDiv}>clone</Button>
      {clones}
    </div>
  );
}

// import React,{useState} from 'react'
// import {Button} from 'react-bootstrap';

// export default function Task14() {
//     const [plus, setPlus] = useState(0);
//     const [clone,setClone] = useState('');

//     const element =
//                     <>
//                     <Button onClick={(e)=>{setPlus(plus+1)}}>+</Button>
//                     <input type="text" className='text-center' value={plus}/>
//                     <Button onClick={(e)=>{setPlus(plus-1)}} >-</Button>
//                     </>;
//     console.log("aa",clone);
//     function cloneDiv(){
//         setClone(element,{});
//     }
//   return (
//     <div>
//     <div>Task14</div>
//         <Button onClick={cloneDiv}>clone</Button>
//         <h2>{clone}</h2>
//     </div>
//   )
// }
