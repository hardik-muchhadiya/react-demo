import React, { useState, useEffect } from "react";
import "./movieDetail.css";
import axios from "axios";
import { MOVIEDB_API_KEY,MOVIEDB_BASE_URL,MOVIEDB_IMG_URL,MOVIEDB_BACK_DROP } from "../utils/constant";

import { useSearchParams } from "react-router-dom";
export default function MovieDetail(props) {
  const [data, setData] = useState("");
  let [searchParams] = useSearchParams();
  const id = searchParams.get("id");
  useEffect(() => {
    fetch(
      `https://api.themoviedb.org/3/movie/${id}?api_key=cc5c5420844b76ee6b52ca24d7e59340&language=en-US//`
    )
      .then((response) => response.json())
      .then((res) => setData(res));
  }, [id]);
  return (
    <>
      <section className="movieDetail">
        <div className="backdrop_img" style={{backgroundImage:`url(${MOVIEDB_BACK_DROP + data.backdrop_path})`}}>
            <div className="maindiv row">
                    <div className="col-6 image">
                        <img src={MOVIEDB_IMG_URL + data.poster_path} alt="error" className="img" />
                    </div>
                    <div className="col-6 detail">
                        <h2><b>Movies:{data.title}</b></h2><br/>
                        <p>Overview:{data.overview}</p><br/>
                        <p >vote_average:{data.vote_average}</p>
                        <h2><b>status:</b>{data.status}</h2>
                    </div>
            </div>
        </div>
      </section>
    </>
  );
}
