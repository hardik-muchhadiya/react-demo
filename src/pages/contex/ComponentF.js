import React, { useContext } from 'react'
import { UserConext,UserConext1 } from '../../App'
export default function ComponentF() {
    const user = useContext(UserConext);
    const user1=useContext(UserConext1);
  return (
        <div>
            {user} ComponentF
            {user1}     
        </div>
  )
}
