import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { checkInput } from "../Redux/Redux-toolkit/checkBoxSlice";
import TinputBox from "./TinputBox";
import TradioBox from "./TradioBox";
import TselectBox from "./TselectBox";
export default function TcheckBox() {
  const checkedValueState = useSelector((state) => state.check.value);
//   console.log("checkedValueState:", checkedValueState);
  const dispatch = useDispatch();
  return (
    <>
      {
        <div style={{ marginLeft: "500px" }}>
          <input
            type="checkbox"
            name="checkbox1"
            checked={checkedValueState.first}
            onClick={(e) =>
              dispatch(
                checkInput({ ...checkedValueState, first: e.target.checked })
              )
            }
          />
          IOs
          <br />
          <input
            type="checkbox"
            name="checkbox2"
            checked={checkedValueState.second}
            onClick={(e) =>
              dispatch(
                checkInput({ ...checkedValueState, second: e.target.checked })
              )
            }
          />
          angular
          <br />
          <input
            type="checkbox"
            name="checkbox3"
            checked={checkedValueState.third}
            onClick={(e) =>
              dispatch(
                checkInput({ ...checkedValueState, third: e.target.checked })
              )
            }
          />
          android <br />
          <h3>duplicate</h3>
          <input
            type="checkbox"
            name="checkbox4"
            checked={checkedValueState.first}
            onClick={(e) =>
              dispatch(
                checkInput({ ...checkedValueState, first: e.target.checked })
              )
            }
          />
          IOs
          <br />
          <input
            type="checkbox"
            name="checkbox5"
            checked={checkedValueState.second}
            onClick={(e) =>
              dispatch(
                checkInput({ ...checkedValueState, second: e.target.checked })
              )
            }
          />
          angular
          <br />
          <input
            type="checkbox"
            name="checkbox6"
            checked={checkedValueState.third}
            onClick={(e) =>
              dispatch(
                checkInput({ ...checkedValueState, third: e.target.checked })
              )
            }
          />
          android <br />
          <hr />
          <TradioBox/>
          <TselectBox/>
          <TinputBox/>
        </div>
      }
    </>
  );
}
