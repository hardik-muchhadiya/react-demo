import React from "react";
import moment from "moment";
import axios from "axios";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faFacebook, faInstagram} from "@fortawesome/free-brands-svg-icons"
import { faCoffee,faGift } from "@fortawesome/free-solid-svg-icons";
export default function Package() {
  axios
    .get("https://reqres.in/api/users?page=1")
    .then(function (response) {
      // handle success
      console.log(response);
    })
    .catch(function (error) {
      // handle error
      console.log(error);
    })
    .then(function () {
      // always executed
    });
  console.log(moment().format("MMMM Do YYYY, h:mm:ss a"));
  console.log(moment("20111031", "YYYYMMDD").fromNow());
  return (
    <>
      <FontAwesomeIcon icon={faCoffee} /><br/>
      <FontAwesomeIcon icon="fa-solid fa-check-square" /><br/>
      <FontAwesomeIcon icon={faGift} /><br/>
      <FontAwesomeIcon icon="fa-regular fa-coffee" /> <br/>
      <i class="fa fa-facebook-square" aria-hidden="true"></i><br/>
      <FontAwesomeIcon icon={faInstagram} /><br/>
      <div> Package</div>
      <FontAwesomeIcon icon="fa-brands fa-instagram" />
    </>
  );
}
