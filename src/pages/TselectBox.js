import React from "react";
import { useSelector, useDispatch } from 'react-redux'
import {selected} from '../Redux/Redux-toolkit/selectBoxSlice'
export default function TselectBox() {
    const selectValueState = useSelector((state) => state.option.value);
    const dispatch = useDispatch();
    console.log("svs:",selectValueState);
  return (
    <>
      <hr />
      <div >
        <select onChange={(e)=>dispatch(selected({...selectValueState,value:e.target.value}))} value={selectValueState.value}>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
        </select>
        <h2>duplicate</h2>
      </div>
      <div >
        <select value={selectValueState.value}>
          <option value="1" >1</option>
          <option value="2">2</option>
          <option value="3">3</option>
        </select>
      </div>
    </>
  );
}
