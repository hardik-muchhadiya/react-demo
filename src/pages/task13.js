import React,{useState} from 'react'
import {Button} from 'react-bootstrap';
export default function Task13() {
    const [plus, setPlus] = useState(0);

    console.log("aa",plus);
  return (
    <div>
        <div>Task13</div>
        <Button onClick={(e)=>{setPlus(plus+1)}}>+</Button>
        <input type="text" className='text-center' value={plus}/>
        <Button onClick={(e)=>{setPlus(plus-1)}} >-</Button>
    </div>
  )
}
