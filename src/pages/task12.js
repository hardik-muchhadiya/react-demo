import React, { useState } from "react";

export default function Task12() {
  const [option, setOption] = useState("temp");
  const [celsius, setCelsius] = useState('');
  const [fahrenheit, setFahrenheit] = useState('');
  const [second, setSecond] = useState('');
  const [minute, setMinute] = useState('');




  console.log('celsius:',celsius);
  console.log('fahrenheit:',fahrenheit);

    function toMinute(time){
        setMinute(time/60);
        setSecond(time);
    }
    function toSecond(time){
        setSecond(time*60);
        setMinute(time);
    }
  function toCelsius(value) {
     setCelsius((value - 32) * 5 / 9);
     setFahrenheit(value);
  }
  
  function toFahrenheit(value) {
    // setCelsius((value * 9 / 5) + 32);
     setFahrenheit(value*(9/5)+32);
     setCelsius(value);
  }


  function selectOption(event) {
    setOption(event.target.value);
  }
  return (
    <>
      <div>Task12</div>
      <br />
      <br />
      <h1>{option}</h1>
      <select onChange={selectOption}>
        <option value="select">Select</option>
        {/* <option value="speed">Speed</option> */}
        <option value="time">Time</option>1
        <option value="temp">Temperature</option>
      </select>
      <br />
      <br />
      <br />

      {option === "temp" && (
        <div style={{ display: "inline-flex" }}>
          <div>
            {/* <input type="number" name="celsius"  onChange={(e)=>{setCelsius(e.target.value)}}/> */}
            <input type="number" name="celsius"  value={celsius} onChange={(e)=>{toFahrenheit(e.target.value)}}/>

            <div style={{ border: "1px solid" }}>celsius</div>
          </div>
          <h1>=</h1>
          <div>
            <input type="number" name="fahrenheit" value={fahrenheit} onChange={(e)=>{toCelsius(e.target.value)}} />
            <div style={{ border: "1px solid" }}>fahrenheit</div>
          </div>
        </div>
      )}

      <br />
      <br />

      {option === "time" && (
        <div style={{ display: "inline-flex" }}>
          <div>
            <input type="number" value={second} onChange={(e)=>{toMinute(e.target.value)}} />
            <div style={{ border: "1px solid" }}>second</div>
          </div>
          <h1>=</h1>
          <div>
            <input type="number" value={minute} onChange={(e)=>{toSecond(e.target.value)}} />
            <div style={{ border: "1px solid" }}>Minute</div>
          </div>
        </div>
      )}
    </>
  );
}
