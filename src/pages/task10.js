import React,{useState} from 'react'
import {Button} from 'react-bootstrap';
export default function Task10() {
    const [heading,setHeading] = useState(true);

    // function button_1(prep){

    //     setHeading(!heading);
    // }
    // console.log('ggs',heading);

    return (
        <>
            <div>Task10</div>
            <br/>
            {/* {heading?<h3>Logistic infotech</h3>:<h3> </h3>} */}
            { heading && <h3>Logistic infotech</h3>}
            <Button variant="primary" onClick={()=>{setHeading(false)}} size="lg">Hide</Button>{' '}
            <Button variant="primary" onClick={()=>{setHeading(true)}}  size="lg">Show</Button>

        </>
    )
}

