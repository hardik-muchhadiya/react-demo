import React from 'react'
import './main.css';
import {Button} from 'react-bootstrap';

export default function Main() {
    const myConst ={name:"hardik",surname:'muchhadiya'};
    const myStyle = {
      color: "white",
      backgroundColor: "DodgerBlue",
      padding: "10px",
      fontFamily: "Sans-Serif"
    };
    return (
    <>
      <h1 className='title'>This is my first blog</h1>
      <p style={myStyle}>hello, my name is {myConst.name} {myConst.surname}</p>
      <Button variant="outline-primary" size="sm" onClick={()=>alert('hello react')}>on click</Button>
    </>
  )
}
