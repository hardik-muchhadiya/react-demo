import React,{useContext} from 'react'
import { Text } from "./ParentI";

export default function ChildI() {
    const {text,setText} = useContext(Text);
  return (<>
            <h1>component</h1>
            <input type="text" placeholder='enter a text' value={text} onChange={(e)=>setText(e.target.value)} />
        </>

  )
}
