// import { setSelect } from '@testing-library/user-event/dist/utils'
import React, { useState } from 'react'
import ChildS from './ChildS'
export const selecContect = React.createContext();

export default function ParentS() {
    // const[select,setSelect]=useState(false);
    const [change,setChanged]=useState(1);
    function changed(event){
        // console.log('ddd',event.target.value);
        // setSelect(true);
        setChanged(event.target.value);

    }
    console.log('change:',change);
  return (
    <>
    <select onChange={changed}  value={change}>
        <option value="1" >1</option>
        <option value="2" >2</option>
        <option value="3" >3</option>

    </select>    
    <hr/>
    <selecContect.Provider value={{change,setChanged}}> 
      <ChildS />
    </selecContect.Provider>
    </>
  )
}
