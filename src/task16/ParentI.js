import React,{useState} from 'react'
import ChildI from './ChildI'
export const Text = React.createContext();

export default function ParentI() {
    const [text,setText]=useState('');

  return (
    <> 
    <input type="text" placeholder='enter a text' value={text} onChange={(e)=>setText(e.target.value)}/>
    <Text.Provider value={{text,setText}}>
        <ChildI />
    </Text.Provider>
    </>
  )
}
