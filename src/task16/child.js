import React ,{useContext}from 'react'
import { checked1 } from './parent'
export default function Child() {
    const {checked,setChecked,checked2,setChecked2,checked3,setChecked3} = useContext(checked1);
  return (
    <div>
        <input type="checkbox" checked={checked}  onClick={()=>setChecked(!checked)} />IOs<br/>
        <input type="checkbox"  checked={checked2}  onClick={()=>setChecked2(!checked2)}/>angular<br/>
        <input type="checkbox"  checked={checked3}  onClick={()=>setChecked3(!checked3)}/>android <br/>      
    
    </div>
  )
}
