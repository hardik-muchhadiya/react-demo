import React, { useContext } from "react";
import { selecContect } from "./ParentS";
export default function ChildS() {
    const {change,setChanged} = useContext(selecContect);
    function ChangeSelect(event){
        console.log('first',event.target.value);
        setChanged(event.target.value); 
    }

    return (
    <select onChange={ChangeSelect} value={change}>
      <option value="1" >1</option>
      <option value="2" >2</option>
      <option value="3">3</option>
    </select>
  );
}
