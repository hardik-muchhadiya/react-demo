import React,{useState} from "react";
import ChildR from "./ChildR";
export const radio1 = React.createContext();
export default function ParentR() {
    const[radio,setRadio]=useState(false);
    const[radio2,setRadio2]=useState(false);

  return (
    <>
      <input type="radio" name="role" checked={radio} onClick={()=>setRadio(!radio)} />
      Developer
      <br />
      <input type="radio" name="role" checked={radio2} onClick={()=>setRadio2(!radio2)} />
      Designer
      <br />
      <radio1.Provider value={{radio,setRadio,radio2,setRadio2}}>
        <ChildR />
      </radio1.Provider>
    </>
  );
}
