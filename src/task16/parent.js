import React, { useState } from "react";
import Child from "./child";
export const checked1 = React.createContext();

export default function Parent() {
  const [checked, setChecked] = useState(false);
  const [checked2, setChecked2] = useState(false);
  const [checked3, setChecked3] = useState(false);

  return (
    <>
    <div style={{ marginLeft: "500px" }}>
      <input
        type="checkbox"
        checked={checked}
        onClick={() => setChecked(!checked)}
      />
      IOs
      <br />
      <input
        type="checkbox"
        checked={checked2}
        onClick={() => setChecked2(!checked2)}
      />
      angular
      <br />
      <input
        type="checkbox"
        checked={checked3}
        onClick={() => setChecked3(!checked3)}
      />
      android <br />
      <hr />
      <checked1.Provider
        value={{
          checked,
          setChecked,
          checked2,
          setChecked2,
          checked3,
          setChecked3,
        }}
      >
        <Child />
      </checked1.Provider>
    </div>
    <hr/>
    </>
  );
}
