import React,{useContext} from "react";
import { radio1 } from "./ParentR";
export default function ChildR() {
    const {radio,setRadio,radio2,setRadio2} = useContext(radio1);
  return (
    <>
      <h2>duplicate chile component</h2>
      <input type="radio" name="crole" checked={radio} onClick={()=>setRadio(!radio)} />
      Developer
      <br />
      <input type="radio" name="crole" checked={radio2} onClick={()=>setRadio2(!radio2)} />
      Designer
    </>
  );
}
