import { configureStore } from "@reduxjs/toolkit";
import counterReducer from "./Redux/Redux-toolkit/counterSlice";
import checkReducers from "./Redux/Redux-toolkit/checkBoxSlice"
import radioRedusers from "./Redux/Redux-toolkit/radioBoxSlice"
import optionReducers from "./Redux/Redux-toolkit/selectBoxSlice"
import inputReducers from "./Redux/Redux-toolkit/inputBoxSlice"
export const toolkitStore = configureStore({

    reducer: {
        counter: counterReducer,
        check:checkReducers,
        radio:radioRedusers,
        option:optionReducers,
        input:inputReducers
    },
  })