import Movie from "../pages/moviedb/movie";
import MovieDetail from "../pages/MovieDetail";
import Task14 from "../pages/task14";
import Parent from "../task16/parent";
import ParentR from "../task16/ParentR";
import ParentS from "../task16/ParentS";
import Parenti from '../task16/ParentI'
import CheckBox from "../pages/redux16/CheckBox";
import Counter from "../pages/Counter";
import TcheckBox from "../pages/TcheckBox";

export const Router = [
    {
        path:'/movie',
        component:<Movie/>
    },
    {
        path:'/movieDetail',
        component:<MovieDetail/>
    },
    {
        path:'/task14',
        component:<Task14/>
    },
    {   //task16 checkbox
        path:'/',
        component:<Parent />

    }
    ,
    {
        path:'/radio',
        component:<ParentR />        
    },
    {
        path:'/select',
        component:<ParentS />
    },
    {
        path:'input',
        component:<Parenti />
    },
    {
        path:'redux-checkbox',
        component:<CheckBox />
    },
    // redux toolkit increament decreament  example
    {   
        path:'redux-toolkit',
        component:<Counter/>
    },
    {
        path:'toolkit-checkbox',
        component:<TcheckBox />
    }

];