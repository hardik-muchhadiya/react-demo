import logo from "./logo.svg";
import "./App.css";
import React from "react";
import Main from "./component/main";
import List from "./component/list";
import Task9 from "./pages/task9";
import Task10 from "./pages/task10";
import Task11 from "./pages/task11";
import Task12 from "./pages/task12";
import Task13 from "./pages/task13";
import Task14 from "./pages/task14";
import MovieDetail from "./pages/MovieDetail";
import {Router} from './routes/route'
import moment from "moment";
import Package from "./pages/package";
import PaginatedItems from "./pages/pagination";
import Movie from "./pages/moviedb/movie";
import {Link} from 'react-router-dom';
import ReactDOM from "react-dom/client";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import L1 from "./pages/l1";
import ComponentF from "./pages/contex/ComponentF";
import Parent from "./task16/parent";
export  const UserConext = React.createContext()
export  const UserConext1 = React.createContext()

        
function App() {

  return (
    <div className="App">
      {/* <Main /> */}
      {/* <List /> */}
      {/* <Task9 /> */}
      {/* <Task10 /> */}
      {/* <Task11/> */}
      {/* <Task12 /> */}
      {/* <Task13 /> */}
      {/* <Task14 /> */}
      {/* <Package /> */}
      {/* <PaginatedItems itemsPerPage={4}/> */}
      {/* <L1 /> */}
      {/* <Movie /> */}
      {/* <Parent /> */}
      <UserConext.Provider value="Vishvash"   >
        <UserConext1.Provider value="hardik">
          
          {/* <ComponentF /> */}
        </UserConext1.Provider>
      </UserConext.Provider>
      <BrowserRouter>
        <Routes>
          
          {
          Router.map((r)=>(
            <Route path={r.path} element={r.component}/>
          ))
          
          }
        </Routes>
      </BrowserRouter>
          
    </div>
  );
}

export default App;
